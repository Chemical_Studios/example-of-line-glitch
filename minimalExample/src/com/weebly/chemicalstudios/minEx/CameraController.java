package com.weebly.chemicalstudios.minEx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class CameraController {
	int x, y, speed = 60 * 2;
	Vector2 vel = new Vector2();
	
	
	public CameraController(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void update(float delta) {
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			vel.x = -speed;
		} else if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			vel.x = speed;
		} else if(Gdx.input.isKeyPressed(Keys.UP)) {
			vel.y = speed;
		} else if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			vel.y = -speed;
		} else {
			vel.x = 0;
			vel.y = 0;
		}
		
		x += vel.x * delta;
		y += vel.y * delta;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
}
