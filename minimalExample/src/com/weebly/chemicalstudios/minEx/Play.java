package com.weebly.chemicalstudios.minEx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Play implements Screen {

	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;

	private OrthographicCamera camera;

	private int[] background = new int[] {0};
	
	private int zoom = 4;
	
	private CameraController controller;
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.position.set(controller.getX(), controller.getY(), 0);
		camera.update();

		renderer.setView(camera);

		renderer.render(background);
		
		renderer.getSpriteBatch().begin();
		
		
		renderer.getSpriteBatch().end();
		
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		camera.viewportWidth = width / zoom;
		camera.viewportHeight = height / zoom;
		
		controller.update(delta);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		TmxMapLoader loader = new TmxMapLoader();


		map = loader.load("data/ExampleMap.tmx");

		renderer = new OrthogonalTiledMapRenderer(map);


		camera = new OrthographicCamera();
		
		controller = new CameraController(0, 0);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
